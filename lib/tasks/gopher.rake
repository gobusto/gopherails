# frozen_string_literal: true
task gopher: :environment do
  host = Rails.env.live? ? 'kiniro.uk' : '127.0.0.1'
  path = "#{Rails.root}/app/gophers"
  ActionGopher::Server.new(path, '/dashboard', host: host, port: 7000)
end
