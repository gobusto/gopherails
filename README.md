GopheRails - A Rails app that can also handle Gopher requests
=============================================================

An attempt to handle both `http://` and `gopher://` requests, using Rails 5 and
(a modified version of) [Gopher2000](https://github.com/muffinista/gopher2000).

How it works
------------

It's actually pretty simple:

+ Some Rails-like "ActionGopher" classes are defined in `gopher/action_gopher`.
+ These are used to create controller-like classes, defined in `app/gophers`.
+ Both of the above are autoloaded by Rails - see `config/application.rb`.
+ A `gopher.rake` task is defined in `lib/tasks`

Type `rails gopher` (or maybe `bundle exec rails gopher`) at the Bash prompt to
start a Gopher2000-based server, similar to how `rails server` works.

Gopher2000 notes
----------------

The only modifications made to Gopher2000 are to prevent it from trying to boot
a new Gopher server instance whenever a Rails task (such as `db:migrate`) runs.

The `ActionGopher::Server` class is used to glue the rest of the `ActionGopher`
code to a Gopher2000 server instance. If you don't want to use Gopher2000, just
ignore/delete/modify `action_gopher/server.rb` as necessary.

Routing
-------

Routing is automatic; there's no equivalent to the Rails `routes.rb` file. URLs
are mapped to Gopher "controllers" based on the following rules:

+ Text after the initial `gopher://my.site:70/1` prefix determines the handler.
+ Text after the (optional) `?` character is treated like HTTP GET parameters.
+ Text after the `#` determines which action to call (`index` is the default).

For example, this URL would call the `show` action of `Some::ThingGopher`, with
`{ 'name' => 'Tom', 'age' => '30' }` as the `params` value:

    gopher://my.site:70/1/some/thing?name=Tom&age=30#show

A simple `gopher://my.site` request will be routed to the `index` action of the
"default" Gopher handler; the default handler is specified at server start-up.

Requests
--------

The `request` object passed to "controllers" comes directly from the Gopher2000
server itself, and therefore has the same attributes:

+ `ip_address` is the IP address that made the request.
+ `selector` is the (complete, unmodified) selector string.
+ `input` is the "query" string for Gopher type-7 items, if present.

If you decide to replace Gopher2000 with something else, you'll need to emulate
this somehow.

Rendering
---------

ActionGopher "controllers" have access to the following methods:

+ `params` returns the GET-style parameters hash.
+ `request` returns the Gopher2000-style request object.
+ `render('text')` outputs an `i`-type line of text.
+ `redirect(SomeGopher, 'abc')` calls the `abc` action of `SomeGopher`.

The `render` method has several optional arguments:

+ `kind` specifies the Gopher type to output (`i` by default).
+ `host` specifies which server the selector is for (this server by default).
+ `port` specifies which port to use (the current server port by default).
+ `path` specifies the selector text (`/` by default).
+ `action` specifies the action for that path (implicitly `index` by default).
+ `params` specifies any GET-style params as a hash. This is totally optional.

Also, `index` is used if the `action` parameter of `redirect` isn't specified.

Possible improvements
---------------------

One day, the core Gopher-handling bits might be extracted into a re-usable gem.

It would also be nice to tweak the few bits of code that rely on Rails-specific
functionality (such as `.constantize`) so that independent, Gopher-only servers
can be made with "pure" Ruby.

Licensing
---------

This project uses the [MIT licence](https://github.com/rails/rails/blob/master/MIT-LICENSE),
just like Rails itself.

The Gopher2000 gem is licensed under the [WTFPL](https://github.com/muffinista/gopher2000/blob/master/LICENSE.txt)
