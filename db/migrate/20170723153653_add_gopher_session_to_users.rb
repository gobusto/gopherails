class AddGopherSessionToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :gopher_session, :string
  end
end
