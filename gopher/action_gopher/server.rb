# frozen_string_literal: true

require_relative './support.rb'
require_relative './router.rb'
require_relative './base.rb'
require 'gopher2000/dsl'

module ActionGopher
  # A small class to interface our ActionGopher stuff with the Gopher2000 gem:
  class Server
    include Gopher::DSL

    def initialize(files_path, root, host: '0.0.0.0', port: 70)
      set(:host, host)
      set(:port, port)
      # rubocop:disable Style/GlobalVars
      $gopher_router = ActionGopher::Router.new(files_path, root, host, port)
      default_route do
        result = $gopher_router.route(request)
        ActiveRecord::Base.clear_active_connections! if defined?(ActiveRecord)
        result
      end
      # rubocop:enable Style/GlobalVars
      Gopher::Server.new(@application).run!
    end
  end
end
