# frozen_string_literal: true

require 'uri'

module ActionGopher
  # A base class for Gopher "controllers", similar to ApplicationController.
  class Base
    @@_before_actions = []
    @@_after_actions = []

    def initialize(request, params, host, port)
      @_request = request
      @_params = params
      @_host = host
      @_port = port
      @_menu = []
      @_redirect = nil
    end

    private

    # Add a before-action to the list:
    def self.before_action(action)
      @@_before_actions << { controller: self, action: action }
    end

    # Add an after-action to the list:
    def self.after_action(action)
      @@_after_actions << { controller: self, action: action }
    end

    # Private, to prevent recursion if 'route!' is specified as the action:
    def route!(action)
      run_actions! @@_before_actions
      public_send(action) unless redirected?
      run_actions! @@_after_actions
      render_menu!
    end

    # Run a list of before/after actions:
    def run_actions!(items)
      items = items.select { |item| is_a? item[:controller] }
      items.each { |item| send(item[:action]) unless redirected? }
    end

    # Render the result as a Gopher menu:
    def render_menu!
      return @_redirect unless @_redirect.nil?
      text = @_menu.map do |v|
        "#{v[:type][0]}#{v[:text]}\t#{v[:path]}\t#{v[:host]}\t#{v[:port]}"
      end
      text.join("\r\n")
    end

    def redirected?
      !@_redirect.nil?
    end

    def request
      @_request
    end

    def params
      @_params
    end

    # Add a single Gopher item to the menu output:
    def render(text, options = {})
      options[:text] = text.delete("\t\n\r")
      options[:type] ||= 'i'
      options[:host] ||= @_host
      options[:port] ||= @_port
      options[:path] ||= '/'

      args = options[:params] || {}
      options[:path] += "?#{URI.encode_www_form(args)}" unless args.empty?

      action = options[:action]
      options[:path] += "##{action}" unless action.nil?

      @_menu << options
    end

    # Forward this request on to another ActionGopher handler instead:
    def redirect_to(klass, action = 'index')
      other = klass.new(@_request, @_params, @_host, @_port)
      @_redirect = other.send(:route!, action)
    end
  end
end
