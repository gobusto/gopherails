# frozen_string_literal: true

module ActionGopher
  # Miscellaneous "support" methods.
  module Support
    # Parse `/selector/text?name=Steve&age=25#show` into `path?params#action`:
    def self.parse_request(request)
      selector, _, action = request.selector.partition('#')
      selector, _, params = selector.partition('?')
      action = 'index' if action == ''

      # Convert the parameters into a hash, and collapse any one-item arrays:
      params = CGI.parse(params)
      params.each { |k, _| params[k] = params[k][0] if params[k].is_a?(Array) }

      [selector, params, action]
    end
  end
end
