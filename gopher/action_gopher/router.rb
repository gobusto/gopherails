# frozen_string_literal: true

module ActionGopher
  # Forwards a Gopher request on to the appropriate Gopher "controller" class:
  class Router
    def initialize(base_dir, default_selector, host, port)
      @base_dir = base_dir
      @host = host
      @port = port
      @default_selector = default_selector
    end

    # Handle an individual request.
    def route(request)
      selector, params, action = ActionGopher::Support.parse_request(request)
      selector = @default_selector if selector == '/'

      # Reload any Gopher "controllers" in case they have changed:
      klass = load_controllers(@base_dir)[selector]

      puts "Started GOPHER \"#{selector}\" for #{request.ip_address} at #{DateTime.current}"
      puts "Processing by #{klass || '<unknown>'}##{action} as menu"
      puts "Parameters: #{params}"

      if klass && klass.instance_methods.include?(action.to_sym)
        klass.new(request, params, @host, @port).send(:route!, action)
      else
        "3No route matches #{selector}\tfake\tfake\t70"
      end
    end

    private

    # Load any Gopher "controllers" into memory, ready to be used.
    def load_controllers(base_dir)
      gopher_files = Dir["#{base_dir}/**/*_gopher.rb"]
      gopher_files.each { |file_name| load file_name }

      # NOTE: `.camelcase` and `.constantize` are Railsisms, so you may need to
      # modify this code if you plan to try and use this without Rails present:
      names = gopher_files.map { |f| f.slice((base_dir.size)...(f.size - 10)) }
      Hash[names.map { |f| [f, "#{f.camelcase}Gopher".constantize] }]
    end
  end
end
