# frozen_string_literal: true

require 'securerandom'

# This class handles user creation/login/logout via Gopher, in place of Devise.
class AccountGopher < ActionGopher::Base
  # By default, prompt the user to log in or create an account:
  def index
    render 'YOU ARE NOT LOGGED IN'
    render '---------------------'
    render 'Do you want to log in, or create a new account?'
    render 'Log in', type: '1', path: 'account', action: :login_user
    render 'Create account', type: '1', path: 'account', action: :create_user
  end

  # If they choose to log in, first ask for their email address:
  def login_user
    render 'LOG IN'
    render '------'
    render 'Enter your email', type: '7', path: 'account', action: :login_pass
    render 'Cancel', type: '1', path: 'account'
  end

  # Once we know their email address, ask them for their password:
  def login_pass
    email = request.input.to_s
    if !valid_email?(email)
      render "The email you have entered doesn't appear to be valid", type: '3'
      return login_user
    end

    render 'LOG IN'
    render '------'
    render "You are logging in as #{email}"
    render 'Enter your password', type: '7', path: 'account', action: :login, params: { email: email }
    render 'Change email', type: '1', path: 'account', action: :login_user
  end

  # Once we have both their email address and their password, try to log in:
  def login
    user = User.find_by(email: params['email'])
    if user && !user.access_locked? && user.valid_password?(request.input)
      login_as(user)
    else
      fail_login(user)
    end
  end

  # If they choose to create a new account, firstask for their email address:
  def create_user
    render 'CREATE ACCOUNT'
    render '--------------'
    render 'Enter your email', type: '7', path: 'account', action: :create_pass
    render 'Cancel', type: '1', path: 'account'
  end

  # Once we know their email address, ask them for their password:
  def create_pass(email = request.input.to_s)
    user = User.find_by(email: email)

    if !valid_email?(email)
      render "The email you have entered doesn't appear to be valid", type: '3'
      create_user
    elsif user.present?
      render 'This email address is already registered.', type: '3'
      create_user
    else
      render 'CREATE ACCOUNT'
      render '--------------'
      render "Your email address will be #{email}"
      render 'Enter a password', type: '7', path: 'account', action: :create, params: { email: email }
      render 'Change email', type: '1', path: 'account', action: :create_user
    end
  end

  # Once we have both an email and a password, we can create an account:
  def create
    user = User.create(email: params['email'], password: request.input)
    return login_as(user) if user.persisted?
    # If we couldn't create a user, explain why:
    render user.errors.full_messages.to_sentence, type: '3'
    create_pass(user.email)
  end

  # If a logged-in user wants to log out, delete their previous session key:
  def logout
    user = User.find_by(
      id: params['user'],
      gopher_session: params['session'],
      current_sign_in_ip: request.ip_address
    )

    if user.nil?
      render 'You are not logged in', type: '3'
    else
      user.update(gopher_session: nil)
      render 'LOGGED OUT'
      render '----------'
      render 'You are now logged out.'
    end

    render 'Go to the login page', type: '1', path: 'account'
  end

  private

  # Log the specified user in via Gopher:
  def login_as(user)
    user.update!(
      failed_attempts: 0,
      sign_in_count: user.sign_in_count + 1,
      last_sign_in_at: user.current_sign_in_at,
      last_sign_in_ip: user.current_sign_in_ip,
      current_sign_in_ip: request.ip_address,
      current_sign_in_at: DateTime.current,
      gopher_session: SecureRandom.uuid
    )

    render 'LOGGED IN'
    render '---------'
    render "You are now logged in as #{user}"
    render(
      'Click here to continue',
      type: '1',
      params: { user: user.id, session: user.gopher_session }
    )
  end

  # Update the failed login attempts counter, possibly locking the account:
  def fail_login(user)
    if user && user.failed_attempts < max_fail
      args = { failed_attempts: user.failed_attempts + 1 }
      args[:locked_at] = DateTime.current if args[:failed_attempts] == max_fail
      user.update(args)
    end

    # If the user exists, but is locked, show a message to indicate this:
    return render('Your account is locked', type: '3') if user&.access_locked?

    # Otherwise, just show a generic "wrong user/pass" message instead:
    render 'Unrecognised email or password', type: '3'
    render 'Try again', type: '1', path: 'account', action: :login_user
  end

  # Allow the same number of failed Gopher-login attempts as Devise does:
  def max_fail
    Rails.application.config.devise.maximum_attempts
  end

  # Basic validation to ensure that an email address looks OK:
  def valid_email?(email)
    email.include?('@')
  end
end
