# frozen_string_literal: true
class DashboardGopher < ApplicationGopher
  def index
    render "You are signed in as #{current_user}"
    render "The selector you requested was #{request.selector}"
    render "Your IP is #{request.ip_address}"
    render "Type-7 query is #{request.input}"
    render "GET-style parameters: #{params}"
    render 'log out', type: '1', path: 'account', action: :logout
  end
end
