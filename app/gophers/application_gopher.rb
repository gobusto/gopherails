# frozen_string_literal: true
class ApplicationGopher < ActionGopher::Base
  before_action :authenticate_user!

  protected

  # Once logged in, each request needs to track the user and session ID:
  def render(text, options = {})
    return unless current_user
    extras = { user: current_user.id, session: current_user.gopher_session }
    options[:params] ||= {}
    options[:params].merge!(extras)
    super(text, options)
  end

  # If the user is not logged in, redirect them to the user account screens:
  def authenticate_user!
    @_current_user = User.find_by(
      id: params[:user],
      gopher_session: params[:session],
      current_sign_in_ip: request.ip_address
    )
    return unless @_current_user.nil?
    puts "User is not logged in - redirecting..."
    redirect_to(AccountGopher)
  end

  # Get the currently-logged-in user:
  def current_user
    @_current_user ||= nil
  end

  # Allow either string or symbol keys to be used, as per Rails controllers:
  def params
    HashWithIndifferentAccess.new(super)
  end
end
